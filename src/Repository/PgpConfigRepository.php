<?php

namespace App\Repository;

use App\Entity\PgpConfig;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Service\PgpService;
use Psr\Log\LoggerInterface;

/**
 * @method PgpConfig|null find($id, $lockMode = null, $lockVersion = null)
 * @method PgpConfig|null findOneBy(array $criteria, array $orderBy = null)
 * @method PgpConfig[]    findAll()
 * @method PgpConfig[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PgpConfigRepository extends ServiceEntityRepository
{
    private $pgpService;

    public function __construct(ManagerRegistry $registry, PgpService $pgpService)
    {
        parent::__construct($registry, PgpConfig::class);
        $this->pgpService = $pgpService;
    }

    public function transform(PgpConfig $pgpConfig)
    {
        return [
                'id'    => (int) $pgpConfig->getId(),
                'email' => (string) $pgpConfig->getEmail(),
                'passphrase' => (int) $pgpConfig->getPassphrase()
        ];
    }

    public function transformAll()
    {
        $pgpConfigs = $this->findAll();
        $pgpConfigsArray = [];

        foreach ($pgpConfigs as $pgpConfig) {
            $pgpConfigsArray[] = $this->transform($pgpConfig);
        }

        return $pgpConfigsArray;
    }

    public function getPgpKeys(Request $request, LoggerInterface $logger)
    {
        $data = $request->request->get('pgpkeysRequest');
        $pgpConfig = new PgpConfig();
        $pgpConfig->setId(1);
        $pgpConfig->setEmail($data['email']);
        $pgpConfig->setPassphrase($data['passphrase']);
        $pgpConfig->setLength($data['length']);
        $pgpConfig->setName('PGP Keys');
        $pgpConfig->setPublickey($this->pgpService->getPublicKey($data,$logger));
        $pgpConfig->setPrivatekey($this->pgpService->getPrivateKey($data,$logger));

        return $pgpConfig;
    }

    // /**
    //  * @return PgpConfig[] Returns an array of PgpConfig objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PgpConfig
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
