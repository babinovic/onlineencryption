<?php
namespace App\Controller;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


use App\Entity\PgpConfig;

class MainController extends AbstractController
{ 
    /**
    * @Route("/onlineencryption/home")
    */
    public function home()
    {
        $pgpconfig = new PgpConfig();
        $form = $this->createFormBuilder($pgpconfig)
            ->add('email', TextType::class)
            ->add('passphrase', TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Generate PGP keys'])
            ->getForm();

        return $this->render('pgpkeys/home.html.twig',[
            'form' => $form->createView(),
        ]);
    }
}