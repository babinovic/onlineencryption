<?php
namespace App\Controller\api;

use Symfony\Component\Routing\Annotation\Route;
use App\Controller\api\ApiController;
use App\Repository\PgpConfigRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Entity\PgpConfig;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Psr\Log\LoggerInterface;

class PgpController extends AbstractFOSRestController
{
     /**
     * generer les clés PGP à partir de l'email et de la passphrase
     *
     * @Rest\View(statusCode=Response::HTTP_OK,serializerGroups={"Default"})
     * @Rest\Post("/api/generate")
     * 
     *
     * @return PgpConfig
     */
    public function generateAction(Request $request, LoggerInterface $logger)
    {
        $logger->info('requete recue:'.$request);
        $pgpConfig =  $this->getDoctrine()->getRepository(PgpConfig::class)->getPgpKeys($request,$logger);
        $logger->info('réponse:'.json_encode($pgpConfig));
        return $pgpConfig;
    }
}