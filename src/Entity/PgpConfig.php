<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PgpConfigRepository")
 * 
 */
class PgpConfig
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     * @Groups({""})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     * @Groups({""})
     */
    private $passphrase;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     * @Groups({""})
     */
    private $length;

    /**
     * @ORM\Column(type="text", nullable=true)
     * 
     * @Groups({"Default"})
     */
    private $publickey;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     * @Groups({"Default"})
     */
    private $publickeypath;

    /**
     * @ORM\Column(type="text", nullable=true)
     * 
     * @Groups({"Default"})
     */
    private $privatekey;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     * @Groups({"Default"})
     */
    private $privatekeypath;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     * @Groups({""})
     */
    private $name;

    /**
     * Constructor
     */
    public function __construct()
    {
 
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassphrase(): ?string
    {
        return $this->passphrase;
    }

    public function setPassphrase(string $passphrase): self
    {
        $this->passphrase = $passphrase;

        return $this;
    }

    public function getPublickey(): ?string
    {
        return $this->publickey;
    }

    public function setPublickey(?string $publickey): self
    {
        $this->publickey = $publickey;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPublickeypath(): ?string
    {
        return $this->publickeypath;
    }

    public function setPublickeypath(?string $publickeypath): self
    {
        $this->publickeypath = $publickeypath;

        return $this;
    }

    public function getPrivatekey(): ?string
    {
        return $this->privatekey;
    }

    public function setPrivatekey(?string $privatekey): self
    {
        $this->privatekey = $privatekey;

        return $this;
    }

    public function getPrivatekeypath(): ?string
    {
        return $this->privatekeypath;
    }

    public function setPrivatekeypath(?string $privatekeypath): self
    {
        $this->privatekeypath = $privatekeypath;

        return $this;
    }

    public function getLength(): ?string
    {
        return $this->length;
    }

    public function setLength(string $length): self
    {
        $this->length = $length;

        return $this;
    }
}
