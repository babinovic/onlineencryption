<?php
namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Psr\Log\LoggerInterface;

class PgpService
{
    //retourne une chaine de caractères contenant la clé PGP publique
    public function getPublicKey($data, LoggerInterface $logger)
    {
        //on crée la paire de clés signées
        $keygenScript = $this->createKeygenScript($data);
        $processKeyPair = Process::fromShellCommandline('gpg --gen-key --batch "!script!"');
        $processKeyPair->run(null, ['script' => $keygenScript]);
        if (!$processKeyPair->isSuccessful()) 
        {
            throw new ProcessFailedException($processKeyPair);
        }
        //suppression du fichier de travail une fois les clés signées créées
        unlink($keygenScript);

        //on crée la clé publique
        $processPublic = Process::fromShellCommandline('gpg --armor --export "!email!"');
        $processPublic->run(null, ['email' => $data['email']]);

        $logger->info('public key='. $processPublic->getOutput());
        return $processPublic->getOutput();
    }

    //retourne une chaine de caractères contenant la clé PGP privée
    public function getPrivateKey($data, LoggerInterface $logger)
    {
        
        //on crée la clé privée
        $commandLine = 'gpg --armor --export-secret-keys --passphrase="'.$data['passphrase'].'" --pinentry-mode loopback "'.$data['email'].'" ';
        error_log('commandLine='.$commandLine);
        $processPrivate = Process::fromShellCommandline($commandLine);
        $processPrivate->mustRun();
        if (!$processPrivate->isSuccessful()) 
        {
            throw new ProcessFailedException($processPrivate);
        }

        $logger->info('private key='. $processPrivate->getOutput());
        return $processPrivate->getOutput();
    }

    //crée le fichier de script customisé pour une création de clés unatended
    public function createKeygenScript($data)
    {
        //on crée une copie temporaire du script modèle
        $filesystem = new Filesystem();
        $scriptFile = 'work\genkey'.random_int(0,10000);
        $filesystem->copy('genkey',$scriptFile);

        //on replace les balises du modèle par les valeurs
        $this->customizeScript($scriptFile,$data);
              
        return $scriptFile;       
    }

    public function customizeScript($scriptFile, $data)
    {      
        $content = file_get_contents($scriptFile);
        $name = explode("@", $data['email'])[0];

        $content = str_replace('<NAME>',$name,$content);
        $content = str_replace('<EMAIL>',$data['email'],$content);
        $content = str_replace('<PASSPHRASE>',$data['passphrase'],$content);
        $content = str_replace('<LENGTH>',$data['length'],$content);
        
        file_put_contents($scriptFile, $content);
    }
}